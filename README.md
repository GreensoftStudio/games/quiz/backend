<h1 align="center">Welcome to Quiztool 👋</h1>
<p>
  <a href="documentation">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" target="_blank" />
  </a>
  <a href="https://choosealicense.com/licenses/mpl-2.0/">
    <img alt="License: Mozilla Public License 2.0" src="https://img.shields.io/badge/License-Mozilla Public License 2.0-green.svg" target="_blank" />
  </a>
  <a href="https://twitter.com/LordOfFailling">
    <img alt="Twitter: LordOfFailling" src="https://img.shields.io/twitter/follow/LordOfFailling.svg?style=social" target="_blank" />
  </a>
    <a href="https://twitter.com/ShowCastTV">
      <img alt="Twitter: ShowCastTV" src="https://img.shields.io/twitter/follow/ShowCastTV.svg?style=social" target="_blank" />
    </a>
</p>

> A Webappplication to provide a quiz game to you and your friends

### 🏠 [Homepage](http://quiztool.greensoft-studio.com)

## Install

```sh
composer install
```

## Usage

```sh
php bin/console help
```

## Run tests

```sh
php bin/phpunit
```

## Author

👤 **Paul hagen & Tim Willems**

* Twitter: [@LordOfFailling](https://twitter.com/LordOfFailling)
* Twitter: [@ShowCastTV](https://twitter.com/ShowCastTV)
* Github: [@LordOfFailling](https://github.com/LordOfFailling)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/GreensoftStudio/games/quiz/backend/issues).

## Show your support

Give a ⭐️ if this project helped you!

<a href="https://www.patreon.com/GreensoftStudio">
  <img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
</a>

## 📝 License

Copyright © 2019 [Paul hagen & Tim Willems](https://github.com/LordOfFailling).<br />
This project is [Mozilla Public License 2.0](https://choosealicense.com/licenses/mpl-2.0/) licensed.
